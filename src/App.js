import Horloge from './Horloge';
import './App.css';

function App() {
 
  return (
    <div className="App">
        <Horloge></Horloge> 
    </div>
  );
}

export default App;
