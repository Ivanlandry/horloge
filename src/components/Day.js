import React from "react";

export default class Day extends React.Component{

    constructor(props){
        super(props);
    }

    render(){
        return (
            <h1>{this.props.day}</h1>
        );
    }

}

