import React from 'react';

export default class CDate extends React.Component{

    constructor(props){
        super(props);
    }

    render(){
        return(
            <h3>{this.props.date} {this.props.mois} { this.props.annee }</h3>
        );
    }
}