import React from 'react';

export default class Hour extends React.Component{

    constructor(props){
        super(props);
    }

    render(){
        return(
            <h4>{ this.props.hour < 10 ? '0'+ this.props.hour : this.props.hour }: { this.props.min < 10 ? '0'+ this.props.min :  this.props.min} : { this.props.second < 10 ? '0'+ this.props.second :  this.props.second}</h4>
        );
    }
}