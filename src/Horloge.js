import { useState,useEffect } from "react";
import Day from './components/Day';
import Hour from './components/Hour';
import CDate from './components/Date';
import Moment from "./components/Moment";

const now = new Date();

const tab_moment = ['matin','apres midi','soir'];

const tab_jour =['Dimanche','Lundi','Mardi','Mercredi','Jeudi','Vendredi','Samedi'];

const tab_mois = ['Janvier','Fevrier','Mars','Avril','Mai','Juin','Juillet','Aout','Septembre','Octobre','Novembre','Decembre'];


const Horloge = () =>{
    
    const [hour,setHour] = useState(now.getHours());
    const [min,setMin] = useState(now.getMinutes());
    const [second,setSecond] = useState(now.getSeconds());
    const [day,setDay] = useState(now.getDay());
    const [date,setDate] = useState(now.getDate());
    const [month,setMonth] = useState(now.getMonth());
    const [year,setYear] = useState(now.getFullYear());
  

    const [updateDateTime,setUpdateDateTime] = useState('');
    const [updateDay,setUpdateDay] = useState('');

    const renderMoment = () => {
        if(hour >=5 && hour <12){
            return <Moment moment={tab_moment[0].toUpperCase()}></Moment>;
        }
        if(hour >=12 && hour <18 ){
            return <Moment moment={tab_moment[1].toUpperCase()}></Moment>;
        }
        if(hour >= 18 || hour <5){
            return <Moment moment={tab_moment[2].toUpperCase()}></Moment>;
        }
    }

    const refreshHorloge = () =>{

        if(second == 59){
            if(min==59){
                if(hour==23){
                    setSecond(0);
                    setMin(0);
                    setHour(0); 
                    
                }else{
                    setSecond(0);
                    setMin(0);
                    setHour(hour+1);
                }
            }else{
                setSecond(0);
                setMin(min+1);
            }
        }
        else{
            setSecond(second+1);
        }
    }

    const handleUpdateDateTime = () =>{

        if(updateDateTime!=''){

            setYear(updateDateTime.slice(0,4));

            // verifie si le premier nombre du mois n'est pas zero 
            if(updateDateTime.slice(5,7).indexOf('0')==-1){
                setMonth(updateDateTime.slice(5,7)-1);
            }else{
                setMonth(updateDateTime.slice(5,7).slice(1,2)-1);
            }

            setDate(updateDateTime.slice(8,10));

            // verifie si le premier nombre de l'heure n'est pas zero 
            if(updateDateTime.slice(11,13).slice(0,1).indexOf('0')==-1){
                setHour(Number(updateDateTime.slice(11,13)));
            }else{
                setHour(Number(updateDateTime.slice(11,13).slice(1,2)));
            }

            // verifie si le premier nombre de la minute  n'est pas zero 

            if(updateDateTime.slice(14,16).slice(0,1).indexOf('0')==-1){
                setMin(Number(updateDateTime.slice(14,16)));
            }else{
                setMin(Number(updateDateTime.slice(14,16).slice(1,2)));
            }
        }
    };


    const handleUpdateDay =() =>{
       if(updateDay!=''){
            setDay(updateDay);
       }
    };

    useEffect(() => {
       setTimeout(()=>{
           refreshHorloge();
       },1000);
    });

    return (<>
        <div style={{ border:'7px solid gray',backgroundColor:'black', color:'white' }}>

            <Day day={tab_jour[day].toUpperCase()}></Day>

            {renderMoment()}

            <Hour hour={hour} min={min} second={second}></Hour>

            <CDate date={date} mois={tab_mois[month].toUpperCase()} annee={year}></CDate>

        </div>
        <br/>
        <div>
            <input type='datetime-local' onChange={(event)=>{setUpdateDateTime(event.target.value)}}></input>
             <button onClick={handleUpdateDateTime}>modifier la date et l'heure</button><br/><br/>

             <select onChange={(event)=>{setUpdateDay(event.target.value)}}>
                {tab_jour.map((jour,index)=>(
                     <option value={index}>{jour}</option>
                ))}
             </select>
             <button onClick={handleUpdateDay}>mmodifier le jour</button>
        </div>
    </>);
};

export default Horloge;

